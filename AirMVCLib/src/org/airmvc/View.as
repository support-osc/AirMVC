package org.airmvc 
{
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	/**
	 * 视图管理器
	 * @author WLDragon
	 */
	public class View extends Sprite
	{
		
		public function View() {}
				
		/**
		 * 初始化视图
		 */
		public function init():void { throw new Error("请重写init方法!"); }
		
		/**
		 * 绑定控制器的消息地图
		 * @param	messageMap 消息地图
		 */
		internal function bindMessageMap(messageMap:Dictionary):void
		{
			this.messageMap = messageMap;
			init();
			addListeners();
		}
		
		/**
		 * 添加消息监听
		 */
		protected function addListeners():void { throw new Error("请重写addListeners方法!"); };
		
		/**
		 * 向模块内部控制器发送消息
		 * @param	msg      消息名
		 * @param	... args 参数
		 */
		protected function send(msg:String, ... args):void
		{
			(messageMap[msg] as Function).apply(null,args);
		}
		
		/**
		 * 接收模块内部控制器的消息
		 * @param	msg    消息名
		 * @param	handle 处理函数
		 */
		protected function receive(msg:String,handle:Function):void
		{
			messageMap[msg] = handle;
		}
		
		/**
		 * 取消对消息的接收
		 * @param	msg 消息名
		 */
		protected function cancelReceive(msg:String):void
		{
			messageMap[msg] = null;
			delete messageMap[msg];
		}
		
		/**消息地图*/
		internal var messageMap:Dictionary;
		
	}

}