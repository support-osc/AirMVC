package org.airmvc 
{
	/**
	 * 模块控制类定义配置文件
	 * @author WLDragon 2014-02-06
	 */
	public class ModulesConfig 
	{
		public function ModulesConfig()
		{
			firstModule = "";
			modules = [
				//{def:"",url:Begin},
				//{}
			];
		}
		public var firstModule:String;
		public var modules:Array;
	}

}