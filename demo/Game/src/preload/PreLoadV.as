/**
* @mxmlc -output=bin/assets/modules/preload.swf -external-library-path+=lib/core.swc -debug=false -noplay
*/
package preload 
{
	import global.Lyr;
	import org.airmvc.View;
	import ui.preload.PreLoadUI;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class PreLoadV extends View 
	{
		
		public function PreLoadV() 
		{
		}
		
		override public function init():void 
		{			
			ui = new PreLoadUI();
			addChild(ui);
		}
		
		override protected function addListeners():void 
		{
			receive(PreLoadC.SHOW_OR_HIDE,showOrHide);
			receive(PreLoadC.SET_PROGRESS,handleProgress);
			receive(PreLoadC.DESTROY, handleDestroy);
		}
		
		private function showOrHide():void 
		{
			if (parent)
				visible = !visible;
			else
				Lyr.ui.addChild(this);
		}
		
		private function handleProgress(value:Number):void 
		{
			ui.prgLoad.value = value;
		}
		
		private function handleDestroy():void
		{
			removeChild(ui);
			ui = null;
			parent.removeChild(this);
		}
		
		private var ui:PreLoadUI;
	}

}