package scene 
{
	import global.Nws;
	import org.airmvc.Controller;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class SceneC extends Controller 
	{
		
		public function SceneC() 
		{
			super();
		}
		
		override protected function addListeners():void 
		{
			receive(OPEN_LOGIN,handleOpenLogin);
		}
		
		override protected function startup(... args):void 
		{
			if (!view)
			{
				registerView(new SceneV());
				send(SHOW_OR_HIDE);
			}
		}
		
		private function handleOpenLogin():void 
		{
			//Login模块已经加载，此次发送不会重新加载而是直接运行LoginC的startup
			broadcast(Nws.STARTUP_LOGIN);
		}
		
		static public const SHOW_OR_HIDE:String = "show_or_hide";
		static public const OPEN_LOGIN:String = "open_login";
	}

}