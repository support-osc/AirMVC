/**
* @mxmlc -output=bin/assets/modules/scene.swf -external-library-path+=lib/core.swc -debug=false -noplay
*/
package scene 
{
	import global.Lyr;
	import scene.views.Scene;
	import org.airmvc.View;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class SceneV extends View 
	{
		
		public function SceneV() 
		{
			super();
			
		}
		
		override public function init():void 
		{
			sceneComponent = new Scene();
			addChild(sceneComponent);
		}
		
		override protected function addListeners():void 
		{
			receive(SceneC.SHOW_OR_HIDE,showOrHide);
		}
		
		private function showOrHide():void 
		{
			if (parent)
				visible = !visible;
			else
				Lyr.scene.addChild(this);
		}
		
		private var sceneComponent:Scene;
	}

}