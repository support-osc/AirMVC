/**
* @mxmlc -output=bin/assets/modules/login.swf -external-library-path+=lib/core.swc -debug=false -noplay
*/
package login 
{
	import flash.events.MouseEvent;
	import global.Lyr;
	import login.views.Login;
	import org.airmvc.View;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class LoginV extends View 
	{
		
		public function LoginV() 
		{
			super();
			
		}
		
		override public function init():void 
		{
			lg = new Login(onClick);
			addChild(lg);
		}
		
		override protected function addListeners():void 
		{
			receive(LoginC.SHOW_OR_HIDE,showOrHide);
		}
		
		private function showOrHide():void
		{
			if(parent)
				visible = !visible;
			else
				Lyr.ui.addChild(this);
		}
		
		private function onClick(e:MouseEvent):void
		{
			switch (e.target.name) 
			{
				case "":
					
				break;
				default:
			}
			
			//当点击事件比较多时可以用switch分支，Demo暂时忽略
			send(LoginC.ENTRY_SCENE);
		}
		
		private var lg:Login;
	}

}