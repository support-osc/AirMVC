package login.views 
{
	import flash.events.MouseEvent;
	import login.LoginC;
	import org.airmvc.Dispatcher;
	import ui.login.LoginUI;
	/**
	 * ...
	 * @author WLDragon
	 */
	public class Login extends LoginUI
	{
		
		public function Login(click:Function) 
		{
			clickCallBack = click;
			btnLogin.addEventListener(MouseEvent.CLICK,onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			//通过回调把点击事件转移给View管理器处理
			clickCallBack.call(null,e);
		}
		
		private var clickCallBack:Function;
		
	}

}