package 
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import global.Lyr;
	import org.airmvc.ModulesManager;
	
	[SWF(width="600",height="400",backgroundColor="0x000000")]
	/**
	 * 程序入口
	 * 功能，加载游戏初始化数据及解析自定义数据
	 * @author WLDragon
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//加载模式------------------------------------------------------
		CONFIG::release
		private function init(e:Event = null):void 
		{
			if (loadFlag == 0)
			{
				removeEventListener(Event.ADDED_TO_STAGE, init);
				
				//加载库
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, init);
				loader.load(new URLRequest("assets/library.swf"), new LoaderContext(false, ApplicationDomain.currentDomain));
				loadFlag = 1;
			}
			else if (loadFlag == 1)
			{
				e.target.removeEventListener(Event.COMPLETE, init);
				
				//加载模块配置文件
				var urlLoader:URLLoader = new URLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
				urlLoader.addEventListener(Event.COMPLETE, init);
				urlLoader.load(new URLRequest("assets/ModulesConfig.xml"));
				loadFlag = 2;
			}
			else if (loadFlag == 2)
			{
				e.target.removeEventListener(Event.COMPLETE, init);
				var xml:XML = new XML(e.target.data);
				
				//初始化游戏
				Lyr.init(this);
				App.init(this);
				ModulesManager.init(xml);
			}
		}
		
		//非加载模式----------------------------------------------------
		CONFIG::debug
		private function init(e:Event = null):void 
		{
			Lyr.init(this);
			App.init(this);
			ModulesManager.init(new ModulesConfig(),false);
		}
		
		private var loadFlag:int;
	}
	
}