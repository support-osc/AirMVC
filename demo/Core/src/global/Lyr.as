package global 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	/**
	 * 显示层管理
	 * @author WLDragon
	 */
	public class Lyr 
	{
		static public function init(main:DisplayObject):void
		{
			var s:Stage = main.stage;
			s.addChild(scene);
			s.addChild(ui);
		}
		
		static public const scene:Sprite = new Sprite();
		static public const ui:Sprite = new Sprite();
	}

}