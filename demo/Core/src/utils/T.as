package utils 
{
	import utils.pool.ObjectPool;
	/**
	 * 工具类集合
	 * 使用工具集合类方便工具类编程，不需要考虑静态成员
	 * @author WLDragon 2014-01-12
	 */
	public class T 
	{
		public static var pool:ObjectPool = new ObjectPool();
		
	}

}